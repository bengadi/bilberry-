# Project Title

Two class classifier : Field & Road

## CONTEXT

At Bilberry, a big part of our new problems can be tackle with Computer Vision.
As a majority of Computer Vision problems rely on image datasets (for supervised
machine-learning algorithm) we would like you to handle a dataset of field and road.

### Prerequisites

What things you need to install the software

```
Python , Anaconda , keras
```

### DATA
Data is available ​ here​ .

https://drive.google.com/open?id=1MIQxMC0uRSqSdXeJ1Lu7yJzEIwm9la64

## Running the tests

open the cnn files on run it ( i have used spyder (Python3.6))

## Acknowledgments

* this is a very simple CNN but for this kind of problem give a good accuracy, in our exemple : accuracy=1


